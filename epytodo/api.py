##
## EPITECH PROJECT, 2020
## api
## File description:
## api
##

from flask import Flask, render_template, jsonify, request, session
from datetime import timedelta
import pymysql as sql

app = Flask(__name__)
app.config.from_object('config')
connect = sql.connect(host=app.config['DATABASE_HOST'], unix_socket=app.config['DATABASE_SOCK'], user=app.config['DATABASE_USER'], passwd=app.config['DATABASE_PASS'], db=app.config['DATABASE_NAME'])
app.secret_key = "caca"
app.permanent_session_lifetime = timedelta(minutes = 5)

def get_task_result_json(res: str):
    result: dict = {}
    data: dict = {}
    data['title'] = res[1]
    data['begin'] = res[2]
    data['end'] = res[3]
    data['status'] = res[4]
    result['result'] = data
    return jsonify(result)

def check_user_exists(username: str):
    temp: str = None
    try:
        cursor = connect.cursor()
        cursor.execute("SELECT COUNT(1) FROM user WHERE username = '{}';".format(username))
        temp = cursor.fetchall()
        cursor.close
        connect.close
        if int(temp[0][0]) > 0:
            return True
        else:
            return False
    except Exception as error:
        print(error)

@app.route('/register', methods=['POST'])
def register_user():
    result:dict = {}
    data = request.get_json()
    try:
        username = data['username']
        password = data['password']
        if (check_user_exists(username) == True):
            result['error'] = "account already exists"
            return jsonify(result)
        else:
            cursor = connect.cursor()
            cursor.execute("INSERT INTO user (username, password) VALUES ('{}', '{}');".format(username, password))
            connect.commit()
            cursor.close
            connect.close
            result['result'] = "account created"
            return jsonify(result)
    except Exception as error:
        print(error)
        result['error'] = "internal error"
        return jsonify(result)

def get_task_id(title: str, status: str):
    try:
        cursor = connect.cursor()
        cursor.execute('SELECT task_id FROM task WHERE title = "{}" AND status = "{}";'.format(title, status))
        cursor.close
        connect.close
        result = cursor.fetchone()
        return result[0]
    except Exception as error:
        return jsonify(error)

@app.route('/user/task/add', methods=['POST'])
def create_task():
    result: dict = {}
    data = request.get_json()
    if 'id' in session:
        try:
            title = data['title']
            begin = data['begin']
            end = data['end']
            status = data['status']
            cursor = connect.cursor()
            cursor.execute('INSERT INTO task (title, begin, end, status) VALUES (%s, %s, %s, %s);', (title, begin, end, status))
            connect.commit()
            cursor.close
            connect.close
            task_id = get_task_id(title, status)
            cursor = connect.cursor()
            cursor.execute('INSERT INTO user_has_task (fk_user_id, fk_task_id) VALUES (%s, %s);', (session['id'], task_id))
            connect.commit()
            cursor.close
            connect.close
            result['result'] = "new task added"
            return jsonify(result)
        except Exception as error:
            result['error'] = "internal error"
            print(error)
            return jsonify(result)
    else:
        result['error'] = "you must be logged in"
        return jsonify(result)
    return 0

@app.route('/user/task/<int:id>', methods=['GET'])
def view_task_id(id: int):
    result: str = None
    error: dict = {}
    if 'id' in session:
        try:
            cursor = connect.cursor()
            cursor.execute("SELECT * FROM task WHERE task_id = '{}';".format(id))
            result = cursor.fetchone()
            cursor.close
            connect.close
            if result:
                return get_task_result_json(result)
            else:
                error['error'] = "task id does not exists"
                return jsonify(error)
        except Exception as error:
            print(error)
            error['error'] = "internal error"
            return jsonify(error)
    else:
        error['error'] = "you must be logged in"
        return jsonify(error)

def check_is_correct_password(username: str, password: str):
    cursor = connect.cursor()
    cursor.execute("SELECT COUNT(1) FROM user WHERE username = '{}';".format(username))
    result = cursor.fetchall()
    cursor.close
    connect.close
    if result[0][0] > 0 and get_password(username) == password:
        return True
    else:
        return False

def get_password(username: str):
    cursor = connect.cursor()
    cursor.execute("SELECT password FROM user WHERE username = '{}';".format(username))
    password = cursor.fetchall()
    cursor.close
    connect.close
    return(password[0][0])

def get_user_id(username: str):
    cursor = connect.cursor()
    cursor.execute("SELECT user_id FROM user WHERE username = '{}';".format(username))
    id = cursor.fetchall()
    cursor.close
    connect.close
    return(id[0][0])

@app.route('/user/task', methods=['GET'])
def get_all_task_from_user():
    pre_res: dict = {}
    temp_res: dict = {}
    result: dict = {}
    data: dict = {}
    i = 1;
    if 'id' in session:
        try:
            cursor = connect.cursor()
            cursor.execute('SELECT fk_task_id FROM user_has_task WHERE fk_user_id = {};'.format(session['id']));
            temp = cursor.fetchall()
            cursor.close
            connect.close
            result["result"] = "result"
            for ids in temp:
                cursor = connect.cursor()
                cursor.execute("SELECT * FROM task WHERE task_id = '{}';".format(ids[0]))
                temp = cursor.fetchone()
                cursor.close
                connect.close
                data['title'] = temp[1]
                data['begin'] = temp[2]
                data['end'] = temp[3]
                data['status'] = temp[4]
                pre_res[i] = data
                i += 1
            result = pre_res
            return jsonify(result)
        except Exception as error:
            return jsonify(error)
    else:
        result['error'] = "you must be logged in"
        return jsonify(result)

@app.route('/signin', methods=['POST'])
def signin_user():
    result: dict = {}
    data = request.get_json()
    try:
        username = data['username']
        password = data['password']
        if (check_is_correct_password(username, password) and 'id' not in session):
            session['id'] = get_user_id(username)
            result['result'] = "signin successful"
            return jsonify(result)
        elif 'id' in session:
            result['error'] = "you're already signed"
            return jsonify(result)
        else:
            result['error'] = "login or password does not match"
            return jsonify(result)
    except Exception as error:
        result['error'] = "internal error"
        print(error)
        return jsonify(result)

@app.route('/signout', methods=['POST'])
def signout_user():
    result: dict = {}
    try:
        if 'id' in session:
            session.pop('id', None)
            result['result'] = "signout successful"
            return jsonify(result)
        else:
            result['error'] = "you must be logged in"
            return jsonify(result)
    except Exception as error:
        result['error'] = "internal error"
        return jsonify(result)

def delete_request(id: int):
    result: dict = {}
    try:
        cursor = connect.cursor()
        cursor.execute("DELETE FROM task WHERE task_id = {};".format(id))
        connect.commit()
        cursor.close
        connect.close
        result['result'] = "task deleted"
        return jsonify(result)
    except Exception as error:
        result['error'] = "internal error"
        return jsonify(result)

@app.route('/user/task/del/<int:id>', methods=['POST'])
def delete_task_id(id: int):
    result: dict = {}
    if 'id' in session:
        try:
            cursor = connect.cursor()
            cursor.execute("SELECT * FROM task WHERE task_id = '{}';".format(id))
            data = cursor.fetchone()
            cursor.close
            connect.close
            if data:
                return delete_request(id)
            else:
                result['error'] = "task id does not exists"
                return jsonify(result)
        except Exception as error:
            result['error'] = "internal error"
            return jsonify(result)
    else:
        result['error'] = "you must be logged in"
        return jsonify(result)
    return

def mod_task():
    return 

@app.route('/user/task/<int:id>', methods=['POST'])
def modify_task_id(id: int):
    result: dict = {}
    data = request.get_json()
    if 'id' in session:
        try:
            cursor = connect.cursor()
            cursor.execute("UPDATE task SET title = '{}', begin = '{}', end = '{}', status = '{}' WHERE id = {};".format(id))
            data = cursor.fetchone()
            cursor.close
            connect.close
            if data:
                return mod_task(id)
            else:
                result['error'] = "task id does not exists"
                return jsonify(result)
        except Exception as error:
            result['error'] = "internal error"
            return jsonify(result)
    else:
        result['error'] = "you must be logged in"
        return jsonify(result)