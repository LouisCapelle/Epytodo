#! /usr/bin/env python
from epytodo import app
from epytodo import api

if __name__ == "__main__":
    app.run()